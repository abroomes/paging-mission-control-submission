package com.submission.control.mission.paging.broomes;

import java.util.Objects;

public class AlertKey {
    private final String satelliteId;
    private final String component;
    private final String severity;

    // Constructor
    public AlertKey(String satelliteId, String component, String severity) {
        this.satelliteId = satelliteId;
        this.component = component;
        this.severity = severity;
    }

    // Verify the Object key is the same as the AlertKey
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AlertKey alertKey = (AlertKey) o;
        return Objects.equals(satelliteId, alertKey.satelliteId) &&
                Objects.equals(component, alertKey.component) &&
                Objects.equals(severity, alertKey.severity);
    }

    //Make sure the hashcode is the same as the AlertKey Hashcode
    @Override
    public int hashCode() {
        return Objects.hash(satelliteId, component, severity);
    }
}
