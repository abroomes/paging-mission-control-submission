package com.submission.control.mission.paging.broomes;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class TelemetryChecker {
    public static void main(String[] args) throws IOException, ParseException {
        if (args.length < 1) {
            System.out.println("Please provide the input file as a command line argument.");
            return;
        }

        String inputFile = args[0];
        List<TelemetryData> output = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new FileReader(inputFile));
        String line = reader.readLine();
        Map<AlertKey, Queue<Date>> recentReadings = new HashMap<>();

        SimpleDateFormat inputDateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
        SimpleDateFormat outputDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

        while (line != null) {
            String[] parts = line.split("\\|");

            String timestamp = parts[0];
            int satelliteId = Integer.parseInt(parts[1]);
            double redHighLimit = Double.parseDouble(parts[2]);
            double redLowLimit = Double.parseDouble(parts[5]);
            double rawValue = Double.parseDouble(parts[6]);
            String component = parts[7];
            Date date = inputDateFormat.parse(timestamp);

            String severity = null;
            // Mark all possible alerts
            if (rawValue > redHighLimit) {
                severity = "RED HIGH";
            } else if (rawValue < redLowLimit) {
                severity = "RED LOW";
            }

            if (severity != null) {
                AlertKey key = new AlertKey(String.valueOf(satelliteId), component, severity);   

                Queue<Date> timestamps = recentReadings.getOrDefault(key, new LinkedList<>());
                timestamps.add(date);

                // Remove readings older than 5 minutes
                while (!timestamps.isEmpty() && Math.abs(date.getTime() - timestamps.peek().getTime()) > 5 * 60 * 1000) {
                    timestamps.remove();
                }

                if (timestamps.size() >= 3) {
                    String telemetryTimestamp = outputDateFormat.format(timestamps.peek()); // the timestamp of the first reading

                    TelemetryData record = new TelemetryData(satelliteId, severity, component, telemetryTimestamp);

                    output.add(record);
                    timestamps.clear(); // clear the timestamps once we have added the alert
                }

                recentReadings.put(key, timestamps);
            }

            line = reader.readLine();
        }
        reader.close();

        // Convert the output to JSON Array
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(output);
        System.out.println(json);
    }
}
