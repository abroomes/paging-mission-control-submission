package com.submission.control.mission.paging.broomes;

public class TelemetryData {
    int satelliteId;
    String severity;
    String component;
    String timestamp;

    //constructor
    public TelemetryData(int satelliteId, String severity, String component, String timestamp) {
        this.satelliteId = satelliteId;
        this.severity = severity;
        this.component = component;
        this.timestamp = timestamp;
    }
    // getters and setters

    public int getSatelliteId() {
        return satelliteId;
    }

    public void setSatelliteId(int satelliteId) {
        this.satelliteId = satelliteId;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }


}
